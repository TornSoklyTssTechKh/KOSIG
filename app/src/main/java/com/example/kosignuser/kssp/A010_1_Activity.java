package com.example.kosignuser.kssp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kosignuser.kssp.callback.LogoutYesOrNo;
import com.example.kosignuser.kssp.constant.Constants;
import com.example.kosignuser.kssp.popup.DialogLogout;
import com.example.kosignuser.kssp.tab.MoreFragment;
import com.example.kosignuser.kssp.tran.ComTran;
import com.example.kosignuser.kssp.tx.TX_PRVCD_MBL_P002_REQ;
import com.webcash.sws.pref.MemoryPreferenceDelegator;

public class A010_1_Activity extends AppCompatActivity implements LogoutYesOrNo, View.OnClickListener,ComTran.OnComTranListener{

    private ComTran mComTran;
    TextView userID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a010_1);
        // set ToolBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar_logout);
        setSupportActionBar(toolbar);

        // Initialize ComTran
        mComTran = new ComTran(this,this);
        Button btnLogout= (Button) findViewById(R.id.log_out_butt);
        btnLogout.setOnClickListener(this);

        ImageView ivLeftButtonBack = (ImageView) findViewById(R.id.iv_profil_left_btn);
        ivLeftButtonBack.setOnClickListener(this);

        userID = (TextView) findViewById(R.id.tv_user_nm);
        userID.setText(MemoryPreferenceDelegator.getInstance().get(Constants.LoginInfo.USER_ID));
    }

    @Override
    public void yes() {
        //Toast.makeText(this, "Yes Logout", Toast.LENGTH_SHORT).show();
        try {
            TX_PRVCD_MBL_P002_REQ req = new TX_PRVCD_MBL_P002_REQ();
            req.setUSER_ID(MemoryPreferenceDelegator.getInstance().get(Constants.LoginInfo.USER_ID));
            mComTran.requestData(TX_PRVCD_MBL_P002_REQ.TXNO,req.getSendMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void no() {
        Toast.makeText(this, "Not Logout", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.log_out_butt:
                DialogLogout dialogLogout = new DialogLogout();
                dialogLogout.show(getFragmentManager(),"Logout");
                break;
            case R.id.iv_profil_left_btn:
                Intent intent = new Intent(this, MoreFragment.class);
                finish();
                break;
        }
    }

    @Override
    public void onTranResponse(String tranCode, Object object) {
        if(tranCode.equals(TX_PRVCD_MBL_P002_REQ.TXNO)) {
            // clear id and password
            MemoryPreferenceDelegator.getInstance().put(Constants.LoginInfo.USER_ID,"");
            MemoryPreferenceDelegator.getInstance().put(Constants.LoginInfo.USER_PWD,"");

            Intent intent = new Intent(this,LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }
}
