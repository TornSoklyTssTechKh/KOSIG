package com.example.kosignuser.kssp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.kosignuser.kssp.common.ui.CustomEditTextPassword;
import com.example.kosignuser.kssp.constant.Constants;
import com.example.kosignuser.kssp.tran.ComTran;
import com.example.kosignuser.kssp.tx.TX_PRVCD_MBL_P001_REQ;
import com.example.kosignuser.kssp.tx.TX_PRVCD_MBL_P001_RES;
import com.webcash.sws.pref.MemoryPreferenceDelegator;
import com.webcash.sws.pref.PreferenceDelegator;

import static com.example.kosignuser.kssp.R.id.chk_remember_me;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, ComTran.OnComTranListener{
    private CheckBox chk_remember;
    private EditText et_id,et_pwd;
    private ImageView iv_clear_id,iv_clear_pwd;
    private Button btnLogin;
    ComTran mComTran;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_main);

        // initialize ComTran
        mComTran=new ComTran(this, this);

        // set ToolBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        // back button
        ImageView ivBtnBack = (ImageView) findViewById(R.id.iv_btn_back);
        ivBtnBack.setOnClickListener(this);

        chk_remember = (CheckBox) findViewById(chk_remember_me);
     //   chk_remember.setOnClickListener(this);

        et_id = (EditText) findViewById(R.id.et_id );
        et_pwd= (EditText) findViewById(R.id.et_password);

        iv_clear_id = (ImageView) findViewById(R.id.iv_clear_id);
        iv_clear_pwd = (ImageView) findViewById(R.id.iv_clear_pwd);

        iv_clear_id.setOnClickListener(this);
        iv_clear_pwd.setOnClickListener(this);

        btnLogin = (Button) findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);

        iv_clear_id.setOnClickListener(this);
        iv_clear_pwd.setOnClickListener(this);
        et_id.addTextChangedListener(IdTextWatcher);
        et_pwd.addTextChangedListener(PwdTextWatcher);

        et_id.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && !et_id.getText().toString().equals("")) {
                    iv_clear_id.setVisibility(View.VISIBLE);
                } else {
                    iv_clear_id.setVisibility(View.GONE);
                }

            }
        });

        et_pwd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && !et_pwd.getText().toString().equals("")) {
                    iv_clear_pwd.setVisibility(View.VISIBLE);
                } else {
                    iv_clear_pwd.setVisibility(View.GONE);
                }
            }
        });

        et_pwd.addTextChangedListener(new CustomEditTextPassword(this, et_pwd, 0, CustomEditTextPassword.PASSWORD_NORMAL));
    }
    TextWatcher IdTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(s.toString().equals("")){
                iv_clear_id.setVisibility(View.GONE);
            }else {
                iv_clear_id.setVisibility(View.VISIBLE);
            }
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
        @Override
        public void afterTextChanged(Editable s) {}
    };

    TextWatcher PwdTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(s == null || "".equals(s.toString())) {
                iv_clear_pwd.setVisibility(View.GONE);
                findViewById(R.id.et_passport_hint).setVisibility(View.VISIBLE);
                return;
            }
            findViewById(R.id.et_passport_hint).setVisibility(View.GONE);
            iv_clear_pwd.setVisibility(View.VISIBLE);
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,int after) {}

        @Override
        public void afterTextChanged(Editable s) {}
    };
////

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.iv_btn_back:
                Toast.makeText(this,"Iv_btn_back clicked!",Toast.LENGTH_SHORT).show();
                break;
            case R.id.iv_clear_id:
                et_id.setText("");
                et_id.requestFocus();
                break;
            case R.id.iv_clear_pwd:
                et_pwd.setText("");
                et_pwd.requestFocus();
                break;
            case R.id.btn_login:
                try{
                    TX_PRVCD_MBL_P001_REQ req = new TX_PRVCD_MBL_P001_REQ();
                    req.setUSER_ID(et_id.getText().toString());
                    req.setUSER_PWD(et_pwd.getText().toString());

                    // set id to MemoryPreferenceDelegator when logout we read from it
                    // change this line
                   /* MemoryPreferenceDelegator.getInstance().put("USER_ID",et_id.getText().toString());*/


                    mComTran.requestData(TX_PRVCD_MBL_P001_REQ.TXNO, req.getSendMessage(), false);
                    Log.e("error","Not Err");
                }catch (Exception e) {
                    e.printStackTrace();
                    Log.e("error",e.toString());
                }
                break;
        }
    }

    @Override
    public void onTranResponse(String tranCode, Object object) {
        Log.e("Login"," on try catch Response");
        try{
            if(tranCode.equals(TX_PRVCD_MBL_P001_REQ.TXNO)) {
                TX_PRVCD_MBL_P001_RES res = new TX_PRVCD_MBL_P001_RES(object);
                PreferenceDelegator.getInstance(this).put(Constants.LoginInfo.USER_ID, res.getUSER_ID());
                MemoryPreferenceDelegator.getInstance().put(Constants.LoginInfo.BSNN_NM, res.getBSNN_NM());
                MemoryPreferenceDelegator.getInstance().put(Constants.LoginInfo.CRTC_PATH, res.getCRTC_PATH());
                MemoryPreferenceDelegator.getInstance().put(Constants.LoginInfo.USER_IMG_PATH, res.getUSER_IMG_PATH());
                MemoryPreferenceDelegator.getInstance().put(Constants.LoginInfo.USER_NM, res.getUSER_NM());


          // change here
               if(chk_remember.isChecked())
               {
                   MemoryPreferenceDelegator.getInstance().put(Constants.LoginInfo.USER_PWD, et_pwd.getText().toString());
               }
                Log.e("Login","Response");
                Intent intent= new Intent(this,MainActivity.class);
                startActivity(intent);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }
}
