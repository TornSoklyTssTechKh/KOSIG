package com.example.kosignuser.kssp;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.kosignuser.kssp.adapter.MainViewPagerAdapter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, ViewPager.OnPageChangeListener{

    ViewPager mViewPager;
    MainViewPagerAdapter adapter;
    TextView tv_toolbar_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_toolbar_title = (TextView) findViewById(R.id.tv_toolbar_title);

        mViewPager = (ViewPager) findViewById(R.id.vp_main);
        mViewPager.setOffscreenPageLimit(4);

        adapter = new MainViewPagerAdapter(getSupportFragmentManager(),4);
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(this);

        findViewById(R.id.rl_mycard).setOnClickListener(this);
        findViewById(R.id.rl_receipt).setOnClickListener(this);
        findViewById(R.id.rl_notice).setOnClickListener(this);
        findViewById(R.id.rl_more).setOnClickListener(this);

        setSelectedTab(R.id.rl_mycard);

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch(position) {
            case 0:
                setSelectedTab(R.id.rl_mycard);
               // mainRefreshListener.reloadCard();   //reload card in MyCard
                break;
            case 1:
                setSelectedTab(R.id.rl_receipt);

               // menuClickListener.reloadPage();
                break;
            case 2:
                setSelectedTab(R.id.rl_notice);

              //  noticeRefreshListener.loadNotice();  // Load notice list
                break;
            case 3:
                setSelectedTab(R.id.rl_more);
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void setSelectedTab(int id) {
        if( id == R.id.rl_mycard ) {
            findViewById(R.id.iv_receipt).setSelected(false);
            findViewById(R.id.iv_notice).setSelected(false);
            findViewById(R.id.iv_more).setSelected(false);
            findViewById(R.id.iv_mycard).setSelected(true);

            findViewById(R.id.iv_highligh_mycard).setVisibility(View.VISIBLE);
            findViewById(R.id.iv_highligh_receipt).setVisibility(View.GONE);
            findViewById(R.id.iv_highligh_notice).setVisibility(View.GONE);
            findViewById(R.id.iv_highligh_more).setVisibility(View.GONE);

            tv_toolbar_title.setText("MY카드");

            /*tv_noti_mycard.setText("");
            tv_noti_mycard.setVisibility(View.GONE);*/

        } else if( id == R.id.rl_receipt ) {
            findViewById(R.id.iv_mycard).setSelected(false);
            findViewById(R.id.iv_notice).setSelected(false);
            findViewById(R.id.iv_more).setSelected(false);
            findViewById(R.id.iv_receipt).setSelected(true);

            findViewById(R.id.iv_highligh_mycard).setVisibility(View.GONE);
            findViewById(R.id.iv_highligh_receipt).setVisibility(View.VISIBLE);
            findViewById(R.id.iv_highligh_notice).setVisibility(View.GONE);
            findViewById(R.id.iv_highligh_more).setVisibility(View.GONE);

           /* mTitleBar.setTitleBarType("6");*/
            tv_toolbar_title.setText("사용내역");

//            tv_noti_receipt.setText("");
//            tv_noti_receipt.setVisibility(View.GONE);

        } else if( id == R.id.rl_notice ) {
            findViewById(R.id.iv_receipt).setSelected(false);
            findViewById(R.id.iv_mycard).setSelected(false);
            findViewById(R.id.iv_more).setSelected(false);
            findViewById(R.id.iv_notice).setSelected(true);

            findViewById(R.id.iv_highligh_mycard).setVisibility(View.GONE);
            findViewById(R.id.iv_highligh_receipt).setVisibility(View.GONE);
            findViewById(R.id.iv_highligh_notice).setVisibility(View.VISIBLE);
            findViewById(R.id.iv_highligh_more).setVisibility(View.GONE);

            /*mTitleBar.setTitleBarType("8");*/
            tv_toolbar_title.setText("알림");

//            tv_noti_notice.setText("");
//            tv_noti_notice.setVisibility(View.GONE);

        } else if( id == R.id.rl_more ) {
            findViewById(R.id.iv_receipt).setSelected(false);
            findViewById(R.id.iv_notice).setSelected(false);
            findViewById(R.id.iv_mycard).setSelected(false);
            findViewById(R.id.iv_more).setSelected(true);

            findViewById(R.id.iv_highligh_mycard).setVisibility(View.GONE);
            findViewById(R.id.iv_highligh_receipt).setVisibility(View.GONE);
            findViewById(R.id.iv_highligh_notice).setVisibility(View.GONE);
            findViewById(R.id.iv_highligh_more).setVisibility(View.VISIBLE);

          /*  mTitleBar.setTitleBarType("8");*/
            tv_toolbar_title.setText("더보기");

         /*   tv_noti_more.setText("");
            tv_noti_more.setVisibility(View.GONE);*/
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_mycard:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.rl_receipt:
                mViewPager.setCurrentItem(1);
                break;
            case R.id.rl_notice:
                mViewPager.setCurrentItem(2);
                break;
            case R.id.rl_more:
                mViewPager.setCurrentItem(3);
                break;
        }
        setSelectedTab(v.getId());
    }
}
