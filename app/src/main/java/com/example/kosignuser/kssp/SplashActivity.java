package com.example.kosignuser.kssp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.kosignuser.kssp.conf.Conf;
import com.example.kosignuser.kssp.constant.Constants;
import com.example.kosignuser.kssp.tran.ComTran;
import com.example.kosignuser.kssp.tx.TX_IBK_MG_RES;
import com.example.kosignuser.kssp.tx.TX_IBK_MG_RES_REC1;
import com.webcash.sws.pref.MemoryPreferenceDelegator;

public class SplashActivity extends AppCompatActivity implements ComTran.OnComTranListener{

    ///
    private static final String REQ_MG_DATA 	= "MG_DATA";
    private ComTran mComTran;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mComTran = new ComTran(this,this);
        mComTran.requestData(REQ_MG_DATA , Conf.SITE_MG_URL +"/MgGate?master_id="+Conf.MASTER_ID);

        /// change here

        if(checkAutoLogin())
        {
            int secondsDelayed = 1;
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                }
            }, secondsDelayed * 1500);
        }
    }
    /// change here
    public boolean checkAutoLogin(){
        if(!MemoryPreferenceDelegator.getInstance().get(Constants.LoginInfo.USER_ID).equals("")
                && !MemoryPreferenceDelegator.getInstance().get(Constants.LoginInfo.USER_PWD).equals("")){
            return  true;
        }else {
            return false;
        }
    }
    @Override
    public void onTranResponse(String tranCode, Object object) {
        try {
            if (REQ_MG_DATA.equals(tranCode)) {
                TX_IBK_MG_RES mgRes = new TX_IBK_MG_RES(object);
                boolean strService = mgRes.getAvailableService();
                String serviceReason = mgRes.getAvailableAct();
                if (serviceReason.equals("")) {
                    serviceReason = "안정된 서비스 제공을 위해 시스템 업그레이드가 진행중입니다. 업그레이드 중에는 서비스 이용이 제한됩니다.";
                }
                //+서비스 여부 체크
                if (strService) {

                    TX_IBK_MG_RES_REC1 menuInfo = mgRes.getMenuInfo();

                    Log.e("MGDAGA","REPONSE");

                    //+  MG 정보 저장..
                    MemoryPreferenceDelegator.getInstance().put("MENU_INFO", menuInfo.getMessageToString()); // 앱 메뉴 정보
                    MemoryPreferenceDelegator.getInstance().put("BIZ_URL", mgRes.getBizURL());            // 전문 URL
                    MemoryPreferenceDelegator.getInstance().put("START_URL", mgRes.getStartURL());        // app 시작 URL
                    MemoryPreferenceDelegator.getInstance().put("CHANNEL_ID", mgRes.getChannelID());    // 전문 채널 ID
                    MemoryPreferenceDelegator.getInstance().put("PORTAL_ID", mgRes.getPortalId());        // Portal ID
                    MemoryPreferenceDelegator.getInstance().put("ALIM_URL", mgRes.getALIMURL());        // 알림UR
                    MemoryPreferenceDelegator.getInstance().put("MEMBER_URL", mgRes.getMemberUrl());    // Member URL
                    MemoryPreferenceDelegator.getInstance().put("LOGIN_FORGET_ID", mgRes.getForgetIdURL());    // 로그인 > 아이디 찾기 URL
                    MemoryPreferenceDelegator.getInstance().put("LOGIN_FORGET_PW", mgRes.getForgetPwURL());    // 로그인 > 패스워드 찾기 URL
                    MemoryPreferenceDelegator.getInstance().put("CLOUDE_URI", mgRes.getCloudeUri());    // cloude uri
                    MemoryPreferenceDelegator.getInstance().put("CLOUDE_KEY", mgRes.getCloudeKey());    // cloude api key
                    MemoryPreferenceDelegator.getInstance().put("QNA_URL", mgRes.getQanUrl());            // qna rul
                    MemoryPreferenceDelegator.getInstance().put("EVENT_YN", mgRes.getEventYN());        // event yn
                    MemoryPreferenceDelegator.getInstance().put("EVENT_URL", mgRes.getEventUrl());        // event rul

                    int secondsDelayed = 1;
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                            finish();
                        }
                    }, secondsDelayed * 1500);
                }
            }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    @Override
    public void onTranError(String tranCode, Object object) {

    }
}
