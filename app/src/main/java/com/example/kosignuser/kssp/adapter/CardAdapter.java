package com.example.kosignuser.kssp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kosignuser.kssp.R;
import com.example.kosignuser.kssp.item.CardItem;

import java.util.List;

/**
 * Created by Torn Sokly on 12/29/2016.
 */

public class CardAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<CardItem> cardsList;


    public CardAdapter(Activity activity, List<CardItem> cardsList) {
        this.activity = activity;
        this.cardsList = cardsList;
    }

    @Override
    public int getCount() {
        return cardsList.size();
    }

    @Override
    public Object getItem(int i) {
        return cardsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        try {
            if (inflater == null)
                inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (view == null)
                view = inflater.inflate(R.layout.card_item, null);
            TextView cardButtomTitle = (TextView) view.findViewById(R.id.card_buttom_title);
            ImageView cardNum1 = (ImageView) view.findViewById(R.id.iv_cnum_1);
            ImageView cardNum2 = (ImageView) view.findViewById(R.id.iv_cnum_2);
            ImageView cardNum3 = (ImageView) view.findViewById(R.id.iv_cnum_3);
            ImageView cardNum4 = (ImageView) view.findViewById(R.id.iv_cnum_4);
            ImageView cardNum5 = (ImageView) view.findViewById(R.id.iv_cnum_5);
            ImageView cardNum6 = (ImageView) view.findViewById(R.id.iv_cnum_6);
            ImageView cardNum7 = (ImageView) view.findViewById(R.id.iv_cnum_7);
            ImageView cardNum8 = (ImageView) view.findViewById(R.id.iv_cnum_8);

            final CardItem cardItem = cardsList.get(i);
            cardButtomTitle.setText(cardItem.getCARD_NO());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }
}
