package com.example.kosignuser.kssp.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.kosignuser.kssp.R;

/**
 * Created by kosignUser on 12/28/2016.
 */

public class CustomCardAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    int[] mResources = {
            R.drawable.card2_bs,
            R.drawable.card2_ct,
            R.drawable.card2_hd,
            R.drawable.card2_hn,
            R.drawable.card2_ibk,
            R.drawable.card2_kb
    };


    public CustomCardAdapter(Context Context) {
        this.mContext = Context;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mResources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.card_item,container,false);
        ImageView cardImage = (ImageView) itemView.findViewById(R.id.card_image_view);
        cardImage.setImageResource(mResources[position]);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
