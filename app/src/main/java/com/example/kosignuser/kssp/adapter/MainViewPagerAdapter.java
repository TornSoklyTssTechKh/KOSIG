package com.example.kosignuser.kssp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.kosignuser.kssp.tab.MoreFragment;
import com.example.kosignuser.kssp.tab.MyCardFragment;
import com.example.kosignuser.kssp.tab.MyRecieptFragment;
import com.example.kosignuser.kssp.tab.NoticeFragment;

/**
 * Created by kosignUser on 12/27/2016.
 */

public class MainViewPagerAdapter extends FragmentPagerAdapter {

    int numOfTab;

    public MainViewPagerAdapter(FragmentManager fm, int numOfTab) {
        super(fm);
        this.numOfTab = numOfTab;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0:
                return MyCardFragment.newInstance();
            case 1:
                return MyRecieptFragment.newInstance();
            case 2:
                return NoticeFragment.newInstance();
            default:
                return MoreFragment.newInstance();
        }
    }

    @Override
    public int getCount() {
        return numOfTab;
    }
}
