package com.example.kosignuser.kssp.callback;

/**
 * Created by Torn Sokly on 11/17/2016.
 */

public interface LogoutYesOrNo {
    void yes();
    void no();
}
