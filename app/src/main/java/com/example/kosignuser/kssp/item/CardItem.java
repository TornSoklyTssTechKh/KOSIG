package com.example.kosignuser.kssp.item;

/**
 * Created by kosignUser on 12/29/2016.
 */

public class CardItem {
    private String CARD_CORP_CD;
    private String CARD_NO;
    private String CARD_REG_STS;
    private String CARD_ORG_NM;
    private String CARD_NM;
    private String CARD_BYNM;
    private String CARD_REG_CNT;

    public CardItem(){}


    public CardItem(String CARD_CORP_CD, String CARD_NO, String CARD_REG_STS, String CARD_ORG_NM, String CARD_NM, String CARD_BYNM, String USE_HANDO, String CARD_REG_CNT) {
        this.CARD_CORP_CD = CARD_CORP_CD;

        this.CARD_NO = CARD_NO;
        this.CARD_REG_STS = CARD_REG_STS;
        this.CARD_ORG_NM = CARD_ORG_NM;
        this.CARD_NM = CARD_NM;
        this.CARD_BYNM = CARD_BYNM;
        this.USE_HANDO = USE_HANDO;
        this.CARD_REG_CNT = CARD_REG_CNT;
    }
    public void setCARD_REG_CNT(String CARD_REG_CNT) {
        this.CARD_REG_CNT = CARD_REG_CNT;
    }

    public String getCARD_REG_CNT() {
        return CARD_REG_CNT;

    }
    public String getCARD_CORP_CD() {
        return CARD_CORP_CD;
    }

    public String getCARD_NO() {
        return CARD_NO;
    }

    public void setCARD_CORP_CD(String CARD_CORP_CD) {
        this.CARD_CORP_CD = CARD_CORP_CD;
    }

    public void setCARD_NO(String CARD_NO) {
        this.CARD_NO = CARD_NO;
    }

    public void setCARD_REG_STS(String CARD_REG_STS) {
        this.CARD_REG_STS = CARD_REG_STS;
    }

    public void setCARD_ORG_NM(String CARD_ORG_NM) {
        this.CARD_ORG_NM = CARD_ORG_NM;
    }

    public void setCARD_NM(String CARD_NM) {
        this.CARD_NM = CARD_NM;
    }

    public void setCARD_BYNM(String CARD_BYNM) {
        this.CARD_BYNM = CARD_BYNM;
    }

    public void setUSE_HANDO(String USE_HANDO) {
        this.USE_HANDO = USE_HANDO;
    }

    public String getCARD_REG_STS() {
        return CARD_REG_STS;
    }

    public String getCARD_ORG_NM() {
        return CARD_ORG_NM;
    }

    public String getCARD_NM() {
        return CARD_NM;
    }

    public String getCARD_BYNM() {
        return CARD_BYNM;
    }

    public String getUSE_HANDO() {
        return USE_HANDO;
    }

    private String USE_HANDO;
}
