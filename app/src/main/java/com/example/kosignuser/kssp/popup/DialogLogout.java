package com.example.kosignuser.kssp.popup;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import com.example.kosignuser.kssp.callback.LogoutYesOrNo;

/**
 * Created by Torn Sokly on 12/29/2016.
 */

public class DialogLogout extends DialogFragment {
    LogoutYesOrNo yesOrNo;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        yesOrNo= (LogoutYesOrNo) getActivity();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("로그아웃을 하게 되면 자동 로그인도 해제 됩니다. 정말로 로그아웃하시겠습니까?")
       /*         .setIcon(R.drawable.ic_question_answer_black_24dp)*/
                .setTitle("알림")
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       // Toast.makeText(builder.getContext(),"Yes"+which, Toast.LENGTH_SHORT).show();
                        yesOrNo.yes();
                    }
                })
                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       // Toast.makeText(builder.getContext(),"No"+ which, Toast.LENGTH_SHORT).show();
                        dismiss();
                    }
                });
                /*.setNeutralButton("later!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       // Toast.makeText(builder.getContext(),"Confirm later"+which, Toast.LENGTH_SHORT).show();
                    }
                });*/
        return builder.create();
    }
}
