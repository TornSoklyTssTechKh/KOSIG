package com.example.kosignuser.kssp.tab;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kosignuser.kssp.A010_1_Activity;
import com.example.kosignuser.kssp.R;

/**
 * A simple {@link Fragment} subclass.

 */
public class MoreFragment extends Fragment implements View.OnClickListener{

    private static MoreFragment instance=null;
    public MoreFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v =inflater.inflate(R.layout.fragment_more, container, false);
        (v.findViewById(R.id.option_profile)).setOnClickListener(this);
        // Inflate the layout for this fragment
        return v;
    }
    public static MoreFragment newInstance(){
        try {
            if(instance == null){
                // new instance
                instance = new MoreFragment();

                // sets data to bundle
                Bundle bundle = new Bundle();

                // set data to fragment
                instance.setArguments(bundle);

                return instance;
            } else {

                return instance;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.option_profile:
                startActivity(new Intent(getActivity(), A010_1_Activity.class));
                break;
        }
    }
}
