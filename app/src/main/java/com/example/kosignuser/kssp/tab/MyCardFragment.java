package com.example.kosignuser.kssp.tab;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.kosignuser.kssp.R;
import com.example.kosignuser.kssp.adapter.CardAdapter;
import com.example.kosignuser.kssp.adapter.CustomCardAdapter;
import com.example.kosignuser.kssp.item.CardItem;
import com.example.kosignuser.kssp.tran.ComTran;
import com.example.kosignuser.kssp.tx.TX_PRVCD_MBL_L001_REQ;
import com.example.kosignuser.kssp.tx.TX_PRVCD_MBL_L001_RES;
import com.example.kosignuser.kssp.tx.TX_PRVCD_MBL_L001_RES_REC;
import com.webcash.sws.log.DevLog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyCardFragment extends Fragment implements ViewPager.OnPageChangeListener ,ComTran.OnComTranListener{

    private static MyCardFragment instance = null;
    CustomCardAdapter mCustomCardAdapter;
    Context mContext;
    int numCards;
    ImageView[] imageView;
    private ComTran mComTran;
    private CardAdapter adapter;

    public static List<CardItem> cardList;
    private List<CardItem> display_cardList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // initialize comtran
        mComTran = new ComTran(getActivity(),this);

        mContext = container.getContext();
        View v = inflater.inflate(R.layout.fragment_my_card, container, false);

        mCustomCardAdapter = new CustomCardAdapter(mContext);
        ViewPager mViewPager = (ViewPager) v.findViewById(R.id.pager);
        mViewPager.setClipToPadding(false);
        mViewPager.setPadding(60, 0, 60, 0);
        /*mViewPager.setPageMargin(20);*/
        mViewPager.setOffscreenPageLimit(mCustomCardAdapter.getCount());
        mViewPager.setAdapter(mCustomCardAdapter);
        mViewPager.addOnPageChangeListener(this);

        LinearLayout linearLayout = (LinearLayout) v.findViewById(R.id.not_img_layout);
        numCards = mCustomCardAdapter.getCount();
        imageView = new ImageView[numCards];
        if (numCards > 5) {
            for (int i = 0; i < 5; i++) {
                imageView[i] = new ImageView(mContext);
                if (i == 0) {
                    imageView[i].setImageResource(R.drawable.main_pgn_on_icon);
                } else {
                    imageView[i].setImageResource(R.drawable.main_pgn_off_icon);
                }
                imageView[i].setPadding(5,0,5,0);
                imageView[i].setLayoutParams(new LinearLayout.LayoutParams(20, 20));

                linearLayout.addView(imageView[i]);
            }
        } else {
            for (int i = 0; i < numCards; i++) {
                imageView[i] = new ImageView(mContext);
                if (i == 0) {
                    imageView[i].setImageResource(R.drawable.main_pgn_on_icon);
                } else {
                    imageView[i].setImageResource(R.drawable.main_pgn_off_icon);
                }
                imageView[i].setLayoutParams(new LinearLayout.LayoutParams(15, 15));
                linearLayout.addView(imageView[i]);
            }

        }
        requestCardList();
        return v;
    }

    public void requestCardList(){

        try {
            TX_PRVCD_MBL_L001_REQ req = new TX_PRVCD_MBL_L001_REQ();
            java.text.DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Date date = new Date();
            String sample = dateFormat.format(date);
            String result = sample.replace("/","");
            DevLog.devLog("msg", result);
            req.setINQ_DT(result);
            mComTran.requestData(TX_PRVCD_MBL_L001_REQ.TXNO,req.getSendMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static MyCardFragment newInstance() {

        if (instance == null) {
            // new instance
            instance = new MyCardFragment();

            // sets data to bundle
            Bundle bundle = new Bundle();

            // set data to fragment
            instance.setArguments(bundle);

            return instance;
        } else {

            return instance;
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
       // Toast.makeText(mContext, position + "", Toast.LENGTH_SHORT).show();
        if(position>4){
            //do nothing
        }else{
            for (int i=0;i<5;i++) {
                if(i==position) {
                    imageView[position].setImageResource(R.drawable.main_pgn_on_icon);
                }else {
                    imageView[i].setImageResource(R.drawable.main_pgn_off_icon);
                }
            }
        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTranResponse(String tranCode, Object object) {
            try {
                if(tranCode.equals(TX_PRVCD_MBL_L001_REQ.TXNO)){
                    Log.e("CardResponse",object.toString());
                    TX_PRVCD_MBL_L001_RES res = new TX_PRVCD_MBL_L001_RES(object);
                    TX_PRVCD_MBL_L001_RES_REC rec = res.getREC();
                    cardList.clear();
                    display_cardList.clear();
                    for(int i=0;i < rec.getLength();i++){
                        rec.move(i);
                        CardItem card = new CardItem();
                        card.setCARD_CORP_CD(rec.getCARD_CORP_CD());
                        card.setCARD_NO(rec.getCARD_NO());
                        card.setCARD_REG_STS(rec.getCARD_REG_STS());
                        card.setCARD_ORG_NM(rec.getCARD_ORG_NM());
                        card.setCARD_NM(rec.getCARD_NM());
                        card.setCARD_BYNM(rec.getCARD_BYNM());
                        card.setUSE_HANDO(rec.getUSE_HANDO());
                        card.setCARD_REG_CNT(res.getCARD_REG_CNT());
                        cardList.add(card);
                    }
                    adapter = new CardAdapter(getActivity(),cardList);
                    Log.e("CardResponse",rec.getCARD_NM());
                }
            }catch (Exception e){
                e.printStackTrace();
            }
    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }
}
