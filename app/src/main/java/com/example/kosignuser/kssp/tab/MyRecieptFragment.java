package com.example.kosignuser.kssp.tab;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kosignuser.kssp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyRecieptFragment extends Fragment {

    private static MyRecieptFragment instance = null;

    public MyRecieptFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_reciept, container, false);
    }
    public static MyRecieptFragment newInstance(){

        if(instance == null){
            // new instance
            instance = new MyRecieptFragment();

            // sets data to bundle
            Bundle bundle = new Bundle();

            // set data to fragment
            instance.setArguments(bundle);

            return instance;
        } else {

            return instance;
        }

    }
}
