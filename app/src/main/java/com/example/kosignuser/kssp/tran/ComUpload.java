package com.example.kosignuser.kssp.tran;

import android.content.Context;
import android.graphics.Bitmap;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.webcash.sws.log.DevLog;
import com.webcash.sws.pref.MemoryPreferenceDelegator;
import com.webcash.sws.secure.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.Hashtable;
import java.util.Map;

/*
 * Created by ChanYouvita on 7/5/16.
 */
public class ComUpload {
    private Context mContext;
    private String mUploadUrl = MemoryPreferenceDelegator.getInstance().get("CLOUDE_URI");
    private OnUploadListener mUploadListener;
    private String errmsg, errcd;

    public void setOnCompleted(OnUploadListener listener){
        mUploadListener = listener;
    }

    public ComUpload(Context context){
        mContext = context;
    }

    /*
     * BASE64 convert to binary
     */
    private String getStringImage(Bitmap bmp){
        String encodedImage = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos); //reduce quality of image
            byte[] imageBytes = baos.toByteArray();
            encodedImage = Base64.encodeBytes(imageBytes);
        }catch (Exception e){
            e.printStackTrace();
        }
        return encodedImage;
    }

    /*
     * upload images to cloud server
     */
    public void uploadImage(final UploadItems items){
        try {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, mUploadUrl +"?",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            try {

                                JSONObject jsonObject = new JSONObject(s);
                                errcd = jsonObject.getString("CODE");
                                errmsg = jsonObject.getString("MSG");

                                DevLog.devLog("jsonOut: " + jsonObject.toString());

                                /* 응답 값 */
                                if (!jsonObject.isNull("CODE")){
                                    //* 오류발생 전문 코드값 처리
                                    if (!errcd.equals("0000")){
                                        mUploadListener.onUploadError(errmsg);
                                        return;
                                    }

                                    if (!jsonObject.isNull("FILE_INFO")){
                                        mUploadListener.onUploadCompleted(jsonObject.getJSONArray("FILE_INFO"));
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                mUploadListener.onUploadError(errmsg);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(final VolleyError volleyError) {
                            String errmsg = "사진전송중에 오류가 발생했습니다. 다시 시도해주세요.";
                            mUploadListener.onUploadError(errmsg);
                        }

                    }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    //Converting Bitmap to String
                    String image = getStringImage(items.getFILE());

                    //Creating parameters
                    Map<String, String> params = new Hashtable<>();

                    //Adding parameters
                    params.put("API_KEY", MemoryPreferenceDelegator.getInstance().get("CLOUDE_KEY"));
                    params.put("PTL_ID", items.getPTL_ID());
                    params.put("CHNL_ID", items.getCHNL_ID());
                    params.put("USE_INTT_ID", items.getUSE_INTT_ID());
                    params.put("CNTS_IDNT_ID", "CRD_MAGR_NEW");
                    params.put("USER_ID", items.getUSER_ID());
                    params.put("FILE", image);
                    params.put("FILE_NM", items.getFILE_NM());

                    //returning parameters
                    return params;
                }
            };

            //Creating a Request Queue
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);

            //Adding request to the queue
            requestQueue.add(stringRequest);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /*
     * Method delegate
     */
    public interface OnUploadListener{
        void onUploadCompleted(Object obj);
        void onUploadError(String errmsg);
    }

}
