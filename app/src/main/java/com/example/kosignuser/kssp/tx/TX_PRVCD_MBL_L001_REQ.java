package com.example.kosignuser.kssp.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by kosignUser on 12/26/2016.
 */

public class TX_PRVCD_MBL_L001_REQ extends TxMessage {

    public static final String TXNO = "PRVCD_MBL_L001";

    private TX_PRVCD_MBL_L001_REQ_DATA mTxKeyDATA;

    public TX_PRVCD_MBL_L001_REQ() throws Exception{
        mTxKeyDATA = new TX_PRVCD_MBL_L001_REQ_DATA();
        super.initSendMessage();
    }

    public void setINQ_DT(String value) throws JSONException{
        mSendMessage.put(mTxKeyDATA.INQ_DT,value);
    }

    private class TX_PRVCD_MBL_L001_REQ_DATA{
        private String INQ_DT = "INQ_DT";
    }
}
