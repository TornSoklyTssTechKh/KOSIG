package com.example.kosignuser.kssp.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by kosignUser on 12/26/2016.
 */

public class TX_PRVCD_MBL_L001_RES extends TxMessage {

    private  TX_PRVCD_MBL_L001_RES_DATA mTxData;

    public TX_PRVCD_MBL_L001_RES(Object obj) throws Exception{
        mTxData = new TX_PRVCD_MBL_L001_RES_DATA();
        super.initRecvMessage(obj);
    }

    public String getCARD_REG_CNT() throws JSONException,Exception{
        return getString(mTxData.CARD_REG_CNT);
    }

    public TX_PRVCD_MBL_L001_RES_REC getREC() throws JSONException,Exception{
        return new TX_PRVCD_MBL_L001_RES_REC(getRecord(mTxData.REC));
    }


    private class TX_PRVCD_MBL_L001_RES_DATA{
        private String CARD_REG_CNT = "CARD_REG_CNT";
        private String REC = "REC";
    }
}
