package com.example.kosignuser.kssp.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by kosignUser on 12/26/2016.
 */

public class TX_PRVCD_MBL_L001_RES_REC extends TxMessage {

    private TX_PRVCD_MBL_L001_RES_REC_DATA mTxKeyData;

    public TX_PRVCD_MBL_L001_RES_REC(Object obj) throws Exception {
        mTxKeyData = new TX_PRVCD_MBL_L001_RES_REC_DATA();
        super.initRecvMessage(obj);
    }

    public String getCARD_CORP_CD() throws JSONException,Exception{
        return getString(mTxKeyData.CARD_CORP_CD);
    }

    public String getCARD_NO() throws JSONException,Exception{
        return getString(mTxKeyData.CARD_NO);
    }

    public String getCARD_REG_STS() throws JSONException,Exception{
        return getString(mTxKeyData.CARD_REG_STS);
    }

    public String getCARD_ORG_NM() throws JSONException,Exception{
        return getString(mTxKeyData.CARD_ORG_NM);
    }

    public String getCARD_NM() throws JSONException,Exception{
        return getString(mTxKeyData.CARD_NM);
    }

    public String getCARD_BYNM() throws JSONException,Exception{
        return getString(mTxKeyData.CARD_BYNM);
    }

    public String getUSE_HANDO() throws JSONException,Exception{
        return getString(mTxKeyData.USE_HANDO);
    }

    private class TX_PRVCD_MBL_L001_RES_REC_DATA{
        private String CARD_CORP_CD = "CARD_CORP_CD";
        private String CARD_NO = "CARD_NO";
        private String CARD_REG_STS ="CARD_REG_STS";
        private String CARD_ORG_NM = "CARD_ORG_NM";
        private String CARD_NM = "CARD_NM";
        private String CARD_BYNM = "CARD_BYNM";
        private String USE_HANDO = "USE_HANDO";

    }
}
