package com.example.kosignuser.kssp.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by kosignUser on 12/26/2016.
 */

public class TX_PRVCD_MBL_P001_REQ extends TxMessage {

    public static final String TXNO= "PRVCD_MBL_P001";
    private TX_PRVCD_MBL_P001_REQ_DATA mTxKeyDAta;

    public TX_PRVCD_MBL_P001_REQ() throws Exception{
        super.initSendMessage();
        mTxKeyDAta = new TX_PRVCD_MBL_P001_REQ_DATA();
    }

    public void setUSER_ID(String value) throws JSONException{
        mSendMessage.put(mTxKeyDAta.USER_ID,value); //  put value to HashMap
    }

    public void setUSER_PWD(String value) throws JSONException{
        mSendMessage.put(mTxKeyDAta.USER_PWD,value);
    }

    private class TX_PRVCD_MBL_P001_REQ_DATA{
        private String USER_ID = "USER_ID";    //사용자아이디      it's value act it as key in setter() above
        private String USER_PWD ="USER_PW";   //사용자비밀번호        it's value act it as key in setter() above

    }
}
