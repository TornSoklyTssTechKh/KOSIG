package com.example.kosignuser.kssp.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by kosignUser on 12/26/2016.
 */

public class TX_PRVCD_MBL_P001_RES extends TxMessage {

    private TX_PRVCD_MBL_P001_RES_DATA mTxKeyData;

    public TX_PRVCD_MBL_P001_RES(Object obj) throws Exception{
        mTxKeyData = new TX_PRVCD_MBL_P001_RES_DATA();
        super.initRecvMessage(obj);
    }

    public String getUSER_ID() throws JSONException,Exception{
        return getString(mTxKeyData.USER_ID);
    }

    public String getUSER_NM() throws JSONException,Exception{
        return getString(mTxKeyData.USER_NM);
    }

    public String getUSER_IMG_PATH() throws JSONException,Exception{
        return getString(mTxKeyData.USER_IMG_PATH);
    }

    public String getBSNN_NM() throws JSONException,Exception{
        return getString(mTxKeyData.BSNN_NM);
    }

    public String getCRTC_PATH() throws JSONException,Exception{
        return getString(mTxKeyData.CRTC_PATH);
    }


    private class TX_PRVCD_MBL_P001_RES_DATA{
        private String USER_ID = "USER_ID";
        private String USER_NM = "USER_NM";
        private String USER_IMG_PATH = "USER_IMG_PATH";
        private String BSNN_NM = "BSNN_NM";
        private String CRTC_PATH = "CRTC_PATH";
    }
}
