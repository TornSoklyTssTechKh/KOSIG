package com.example.kosignuser.kssp.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by kosignUser on 12/26/2016.
 */

public class TX_PRVCD_MBL_P002_REQ extends TxMessage {

    public static final String TXNO ="PRVCD_MBL_P002";
    private  TX_PRVCD_MBL_P002_REQ_DATA mTxKeyData;
    public TX_PRVCD_MBL_P002_REQ() throws Exception{
        mTxKeyData = new TX_PRVCD_MBL_P002_REQ_DATA();
        super.initSendMessage();
    }


    public void setUSER_ID(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.USER_ID,value);
    }

    private class TX_PRVCD_MBL_P002_REQ_DATA{
        private String USER_ID ="USER_ID";
    }
}
